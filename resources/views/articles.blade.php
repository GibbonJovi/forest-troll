@extends('app-frontend')

@section('content')
<div class="container-fluid">
	<div class="news-content text-center col-md-3">
	</div>
	<div class="news-content text-center col-md-6">
    @foreach($articles as $article)
		<ul class="list-group">
		  <li class="list-group-item article-title">{{ $article->title }}</li>
		</ul>
		<div class="panel panel-default">
		  <div class="panel-body article-content">
		    {{ $article->content }}
		  </div>
		<span class="label label-default">Published at: {{ $article->published_at }}</span>
		</div>		
    @endforeach
	</div>
</div>
@endsection