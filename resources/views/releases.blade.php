@extends('app-frontend')

<!--  -->

@section('content')

	@foreach($releases as $release)
	<div class="col-md-4"></div>
	<div class="media text-center col-md-6">
	  <div class="media-left">
	    <a href="{{ $release->coverPhoto->cover_path }}">
	      <img class="media-object" src="{{ $release->coverPhoto->cover_path }}">
	    </a>
		<ul class="list-group">
			@foreach($release->tracks->all() as $track)
				<li class="list-group-item">{{ $track->title }} Duration: {{ $track->track_duration }} </li>
			@endforeach	
		</ul>	  
	  </div>
	</div>
    @endforeach
    
@endsection