    <div class="container">
        <header class="navbar navbar-default navbar-static-top lead-navbar" role="banner">
                <div class="row">
                    <div class="col-md-3">
                        <!-- <img class="img img-responsive img-header-navbar" src="../leading practice/images/logo.JPG"> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6">
                        <nav class="collapse navbar-collapse lead-navbar" role="navigation">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a id="objects" href="{{route('articles')}}">
                                    <strong>
                                    News</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="objects" href="{{route('biography')}}">
                                    <strong>
                                    Biography</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="objects" href="{{route('releases')}}">
                                    <strong>
                                    Releases</strong>
                                    </a>
                                </li>
                                <!-- 
                                <li>
                                    <a id="objects" href="#">
                                    <strong>
                                    Pictures</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="objects" href="#">
                                    <strong>
                                    Links</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="objects" href="#">
                                    <strong>
                                    Audio</strong>
                                    </a>
                                </li> -->
                                <li>
                                    <a id="objects" href="#">
                                    <strong>
                                    Contact</strong>
                                    </a>
                                </li>                                                                                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
    </div>