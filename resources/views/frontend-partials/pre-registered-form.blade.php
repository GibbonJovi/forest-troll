{!! Form::Open(['url' => '/pre_register','method' =>'POST', 'class'=>'clearfix well cream-well', 'role'=>'form']) !!}
<h6>
    Pre-register now to get an exclusive discount and preview when we officially launch.
    <img class="pull-right" width="20" alt="" src="/images/brand/Budget-Referee-dollar-circle.png">
</h6>
@if(Session::has('success_message_pre_reg'))
    <div class="text-success validation-summary-valid text-center">
        {{ Session::get('success_message_pre_reg') }}
    </div>
@endif
{{-- Begin Email Field --}}
<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label sr-only']) !!}
    {!! Form::email('email',old('email'),['id'=>'Email','class'=>'form-control','placeholder'=>Lang::get('application.pre_reg_email')]) !!}
    {{-- Begin Email Error --}}
    @if($errors->first('email')!='')
        <span class="field-validation-valid text-danger" data-valmsg-replace="true"
              data-valmsg-for="Email">{{$errors->first('email')}}</span>
    @endif
    {{-- End Email Error --}}
</div>
{{-- End Email Field --}}

<div class="form-group">
    {!! Form::select('budget_type',
     [
        ''=>Lang::get('application.pre_reg_budget_type'),
        'individual' => Lang::get('application.pre_reg_individual_budget'),
        'family' => Lang::get('application.pre_reg_family_budget'),
        'couples' => Lang::get('application.pre_reg_couples_budget')
      ],
     null,
     ['class'=>'form-control','id'=>'BudgetType']) !!}

    @if($errors->first('budget_type')!='')
        <span class="field-validation-valid text-danger" data-valmsg-replace="true"
              data-valmsg-for="BudgetType">{{$errors->first('budget_type')}}</span>
    @endif
</div>
<div class="form-group text-right">
    {!! Form::submit(Lang::get('application.register'),['class'=>'btn btn-primary pull-right']) !!}
    {{--<input class="btn btn-primary" type="submit" value="Register">--}}
</div>
{!! Form::close() !!}