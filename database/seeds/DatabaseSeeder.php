<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //Disable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('ArticlesSeeder');
        $this->call('ReleasesSeeder');
        $this->call('TrackTypesSeeder');
        $this->call('TracksSeeder');
        $this->call('ReleasesCoverSeeder');
        $this->call('BiographiesSeeder');


        //Enable checking Foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
