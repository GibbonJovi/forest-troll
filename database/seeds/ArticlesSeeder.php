<?php
use Illuminate\Database\Seeder;
use App\Article as Article;

class ArticlesSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('articles')->truncate();

        //Create roles
        Article::create([
            'title' => 'Hex released!',
            'content' => 'Today, we just released our first album',
            'published_at' => new \DateTime('2015-06-02 00:00:00')
        ]);

        Article::create([
            'title' => 'Forest Troll was created',
            'content' => 'This is a great day for us, we just created our band!',
            'published_at' => new \DateTime('2015-06-02 00:00:00')
        ]);        

    }
}