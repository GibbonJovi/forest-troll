<?php
use Illuminate\Database\Seeder;
use App\Track as Track;

class TracksSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('biographies')->truncate();

        //Create roles
        Track::create([
            'title' => 'Intro',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Towards The Frozen Peaks',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'The Great Deer',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Trough Pillars and Mists',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Visions Of The Cosmic Void',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Nothing',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Chanting Anthems Across The Realm',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Едно с всичко...',
            'release_id' => '1',
            'track_duration' => 60
        ]);
        Track::create([
            'title' => 'Opus Saltus',
            'release_id' => '1',
            'track_duration' => 60
        ]);

    }
}