<?php
use Illuminate\Database\Seeder;
use App\ReleaseCover as ReleaseCover;

class ReleasesCoverSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('track_types')->truncate();

        //Create roles
        ReleaseCover::create([
            'cover_path' => 'images/hex_album_cover.jpg',
        ]);
            //         $table->increments('id');
            // $table->string('cover_path');
            // $table->string('release_id')->unsigned();
            // $table->timestamps();
    }
}