<?php
use Illuminate\Database\Seeder;
use App\Biography as Biography;

class BiographiesSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('biographies')->truncate();

        //Create roles
        Biography::create([
            'title' => 'Some long time ago',
            'published_at' => new \DateTime('2015-06-02 00:00:00'),
            'content' => 'We had some beers and thought of a band',
        ]);
    }
}