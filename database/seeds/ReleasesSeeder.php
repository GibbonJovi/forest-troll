<?php
use Illuminate\Database\Seeder;
use App\Release as Release;

class ReleasesSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('biographies')->truncate();

        //Create roles
        Release::create([
            'title' => 'HEX',
            'release_date' => new \DateTime('2015-06-02 00:00:00'),
            'release_cover_id' => 1
        ]);
    }
}