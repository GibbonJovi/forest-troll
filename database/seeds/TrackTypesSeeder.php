<?php
use Illuminate\Database\Seeder;
use App\TrackType as TrackType;

class TrackTypesSeeder extends Seeder
{
    public function run()
    {

        //Truncate roles table
        DB::table('track_types')->truncate();

        //Create roles
        TrackType::create([
            'type_name' => 'regular',
        ]);
        TrackType::create([
            'type_name' => 'bonus',
        ]);
    }
}