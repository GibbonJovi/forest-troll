<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('track_duration');
            $table->integer('release_id')->unsigned();
            $table->integer('track_type_id')->unsigned();
            $table->timestamps();
            //
        });

        Schema::table('tracks', function (Blueprint $table) {
            $table->foreign('release_id')->references('id')->on('releases');
            $table->foreign('track_type_id')->references('id')->on('track_types');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tracks');
    }
}
