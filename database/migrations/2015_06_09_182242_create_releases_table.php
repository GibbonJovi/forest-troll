<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->datetime('release_date');
            $table->integer('release_cover_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('releases', function(Blueprint $table) {
            $table->foreign('release_cover_id')->references('id')->on('release_covers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('releases');
    }
}
