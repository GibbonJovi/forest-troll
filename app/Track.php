<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tracks';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'track_duration', 'release_id', 'track_type_id'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function release()
    {
    	return $this->belongsTo('App\Release', 'release_id');
    }

    public function trackType()
    {
    	return  $this->belongsTo('App\TrackType', 'track_type_id');
    }
}
