<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'releases';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'release_date'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function tracks()
    {
        return $this->hasMany('App\Track');
    }

    public function coverPhoto()
    {
        return $this->belongsTo('App\ReleaseCover', 'release_cover_id');
    }
}
