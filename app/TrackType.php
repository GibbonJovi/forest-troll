<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackType extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'track_types';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type_name'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function tracks()
    {
    	return $this->hasMany('App\Track');
    }
}
